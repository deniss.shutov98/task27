export async function fetchCountries() {
  return fetch(`https://restcountries.com/v3.1/all`)
    .then((response) => response.json())
    .then((countries) => {
      return countries;
    });
}

export async function fetchCountriesByRegion(region) {
  return fetch(`https://restcountries.com/v3.1/region/${region}`)
    .then((response) => response.json())
    .then((countries) => {
      return countries;
    });
}

export async function fetchCountriesByName(name) {
  return fetch(`https://restcountries.com/v3.1/name/${name}`)
    .then((response) => response.json())
    .then((countries) => {
      return countries;
    });
}
