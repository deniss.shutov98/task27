import "./styles/styles.css";
import {
  fetchCountries,
  fetchCountriesByRegion,
  fetchCountriesByName,
} from "./scripts/fetch.js";

const countriesContainer = document.getElementById("countriesContainer");
const regionsSelect = document.getElementById("regions-select");
const countrySearch = document.getElementById("country-search");

regionsSelect.addEventListener("change", () => loadData());
countrySearch.addEventListener("input", () => loadData());

loadData();

async function loadData() {
  let region = regionsSelect.options[regionsSelect.selectedIndex].text;
  let name = countrySearch.value;
  let filteredArray;

  if (region == "All" && name == "") {
    filteredArray = await fetchCountries();
  } else if (region != "All" && name == "") {
    filteredArray = await fetchCountriesByRegion(region);
  } else if (region == "All" && name != "") {
    filteredArray = await fetchCountriesByName(name);
  } else {
    let regionsArray = await fetchCountriesByRegion(region);
    let nameArray = await fetchCountriesByName(name);
    filteredArray = getArraysIntersection(regionsArray, nameArray);
  }

  countriesContainer.innerHTML = "";
  for (let country of filteredArray) {
    createCountryContainer(country);
  }
}

function getArraysIntersection(regionsFilteredArray, nameFileterArray) {
  let filteredArray = [];
  for (let element of regionsFilteredArray) {
    for (let secondElement of nameFileterArray) {
      if (element.name.common == secondElement.name.common) {
        filteredArray.push(element);
      }
    }
  }
  return filteredArray;
}

function createCountryContainer(countryData) {
  let container = document.createElement("div");
  container.id = countryData.name.common;
  container.classList.add("country-container");
  container.classList.add(countryData.region);

  addFlagImageToCountryContainer(countryData.flags.png, container);
  addLabelToCountryContainer(`<b>${countryData.name.common}</b>`, container);
  addLabelToCountryContainer(
    `<b>Population:</b> ${countryData.population}`,
    container
  );
  addLabelToCountryContainer(`<b>Region:</b> ${countryData.region}`, container);
  if (countryData.capital !== undefined) {
    addLabelToCountryContainer(
      `<b>Capital:</b> ${countryData.capital[0]}`,
      container
    );
  }

  countriesContainer.appendChild(container);
}

function addFlagImageToCountryContainer(imageSource, container) {
  let image = document.createElement("img");
  image.src = imageSource;
  image.classList.add("country-flag-image");
  container.appendChild(image);
}

function addLabelToCountryContainer(text, container) {
  let label = document.createElement("label");
  label.innerHTML = text;
  container.appendChild(label);
}
